<!doctype html>
<html lang="en">
<head>
    <title>Indonesia Holiday Checker</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <link rel="stylesheet" href="{!! mix('/css/app.css') !!}">
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">

                <h2 class="text-center" style="padding-top: 50px;">Holiday Checker</h2>

                <hr>

                <div class="form-wrapper">

                    <form method="POST" id="formCheck">
                        <div class="form-group">
                            <label for="date">Select Date to Check</label>
                            <input type="date" class="form-control" name="date" id="date" value="{!! Carbon\Carbon::now()->format('Y-m-d') !!}" autofocus>
                        </div>

                        <button class="btn btn-primary" type="submit" id="btnCheck">Check Now</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript" src="{!! mix('/js/app.js') !!}"></script>
    <script>
        HolidayApp();
    </script>
</body>
</html>