// Import sweet alert library.
import swal from 'sweetalert'

export default {

	/**
	 * Check date.
	 * This function will create an ajax request to server 
	 * and display the result in a sweet alert.
	 * 
	 * @param  string date 
	 * @return void
	 */
    check: (date) => {

      $.ajax({
         url: window.location.href + 'check',
         method: 'POST',
         data: {date: date},
         dataType: 'json',
         beforeSend: () => {
            $('#btnCheck').html('Checking...').prop('disabled', true)
        },
    })
      .done((response) => {

			// Display result.
			swal( response.message )

		})
      .fail((error) => {

         if (error.responseJSON.message)
            swal(error.responseJSON.message)
        else
            swal('Oops! Can not process request. Please try again.')

    })
      .always(() => {
         $('#btnCheck').html('Check Now').prop('disabled', false)
     })
  }

}