
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

import HolidayChecker from './holiday-checker'

window.HolidayApp = () => {

	$(document).on('submit', '#formCheck', (e) => {

		e.preventDefault()

		HolidayChecker.check( $('#date').val() )

	})

}
