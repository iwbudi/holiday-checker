<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\HolidayChecker;

use Carbon\Carbon;

class HolidayCheckerTest extends TestCase
{
    protected $apiKey = 'AIzaSyACXDmMP_wOFev-ADcnoiF044N9O0kkZ5s';

    protected $calendarId = 'id.indonesian%23holiday%40group.v.calendar.google.com';

    protected $holidayChecker = null;

    public function __construct()
    {
        parent::__construct();

        $this->holidayChecker = new HolidayChecker($this->calendarId, 
            $this->apiKey);
    }

    /**
     * Test if Calendar API response status returned 200 (OK)
     *
     * @return void
     */
    public function testCalendarApiResponseIsSuccess()
    {
        $this->holidayChecker->getCalendar();

        $response = $this->holidayChecker->getHttpResponse();

        $this->assertTrue($response->getStatusCode() == 200);
    }

    /**
     * Test if input date is invalid.
     * 
     * @return [type] [description]
     */
    public function testInputDateIsInvalid()
    {
        $date = '2017-12-32';

        $this->assertFalse( $this->holidayChecker->dateIsValid($date) );
    }

    /**
     * Test if New Year is a holiday.
     * 
     * @return void
     */
    public function testNewYearIsHoliday()
    {
        $date = '2017-01-01';

        $this->holidayChecker->getCalendar();

        $result = $this->holidayChecker->checkDate($date);

        $this->assertStringEndsNotWith('regular day.', $result);
    }

    /**
     * Test if any random date is a regular day.
     * 
     * @return void
     */
    public function testRandomDateIsRegularDay()
    {
        $date = '2017-09-04';

        $this->holidayChecker->getCalendar();

        $result = $this->holidayChecker->checkDate($date);

        $this->assertStringEndsWith('regular day.', $result);
    }

}
