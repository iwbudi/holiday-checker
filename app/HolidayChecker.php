<?php 

namespace App;

use GuzzleHttp\Client;
use Carbon\Carbon;

use Exception;

/**
* Indonesian Holiday Checker based on Google Calendar API.
* 
*/
class HolidayChecker
{
    /**
     * Calendar ID.
     * 
     * @var string
     */
    protected $calendarId = '';

    /**
     * API Key.
     * 
     * @var string
     */
    protected $apiKey = '';

    /**
     * Method used to access the Calendar API.
     * 
     * @var string
     */
    protected $requestMethod = 'GET';

    /**
     * Base Calendar API URL.
     * 
     * @var string
     */
    protected $apiUrl = 'https://www.googleapis.com/calendar/v3/calendars/%s/events?key=%s';


    /**
     * Http client.
     * 
     * @var null
     */
    protected $httpClient = null;

    /**
     * The Http response.
     * 
     * @var null
     */
    protected $httpResponse = null;

    /**
     * Array of calendar events.
     * 
     * @var array
     */
    protected $calendarEvents = [];

    /**
     * Construct a new HolidayChecker object.
     * 
     * @param string $calendarId the Google Calendar ID
     * @param string $apiKey     the Google API Key
     */
    function __construct($calendarId = '', $apiKey = '')
    {
        $this->calendarId = $calendarId;

        $this->apiKey = $apiKey;

        $httpClient = new Client();
    }

    /**
     * Set the Calendar ID.
     * 
     * @param string $calendarId [description]
     */
    public function setCalendarId($calendarId = '')
    {
        $this->calendarId = $calendarId;
    }

    /**
     * Set API Key that will be used to access the Calendar API.
     * 
     * @param string $apiKey [description]
     */
    public function setApiKey($apiKey = '')
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Build the final calendar API url.
     * 
     * @return string final api url.
     */
    private function buildCalendarApiUrl()
    {
        if ($this->calendarId != '' && $this->apiKey != '') {
            return sprintf($this->apiUrl, $this->calendarId, $this->apiKey);
        }

        throw new Exception('Missing Calendar ID or API Key!');
    }

    /**
     * Get the list of Calendar events.
     * 
     */
    public function getCalendar()
    {
        // Don't fetch again
        if (count($this->calendarEvents) > 0) {
            return;
        }

        if ($this->httpClient == null) {
            $this->httpClient = new Client();
        }

        // Create request to Google Calendar API.
        $this->httpResponse = $this->httpClient->request($this->requestMethod, 
            $this->buildCalendarApiUrl(), [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
            ]]);

        $responseStatusCode = $this->httpResponse->getStatusCode();

        if ($responseStatusCode == 200) {
            
            $responseBody = json_decode( $this->httpResponse->getBody(true)
                ->getContents() );

            // Map calendar events items into array.
            foreach($responseBody->items as $event) {
                $this->calendarEvents[ $event->start->date ] = $event->summary;
            }

        }
        else {
            throw new Exception('Invalid response.');
        }
    }

    /**
     * Check a date.
     * 
     * @param  string $date the date that will be checked
     * @return void 
     */
    public function checkDate($date = '')
    {
        if (count($this->calendarEvents) == 0) {
            $this->getCalendar();
        }

        return $this->check($date);
    }

    /**
     * Check more than one date.
     * 
     * @param  array  $dates array of dates that will be checked
     * @return array $response array of check results.
     */
    public function checkDates($dates = [])
    {
        if (count($this->calendarEvents) == 0) {
            $this->getCalendar();
        }

        $response = [];

        foreach($dates as $date) {
            $response[$date] = $this->check($date);
        }

        return $response;
    }

    /**
     * Real check of the date.
     * 
     * @param  string $date date to check
     * @return string 
     */
    private function check($date = '')
    {
        if ($this->dateIsValid($date)) {

            if (array_key_exists($date, $this->calendarEvents)) {
                return Carbon::parse($date)->format('d F Y') . ' is ' . 
                    $this->calendarEvents[$date] . '.';
            }

            return Carbon::parse($date)->format('d F Y') . ' is a regular day.';
        }

        throw new Exception('Invalid input date.');
    }

    /**
     * Check if given date is valid.
     * 
     * @param  string $date the date to be checked.
     * @return boolean
     */
    public function dateIsValid($date = '')
    {
        return strtotime($date) !== FALSE;
    }

    /**
     * Return the Http response from Guzzle Client.
     * For testing purpose only.
     * 
     * @return Response $httpResponse Guzzle Http Response
     */
    public function getHttpResponse()
    {
        return $this->httpResponse;
    }

    /**
     * Return the array of events.
     * For testing purpose only.
     * 
     * @return array $calendarEvents array of calendar events.
     */
    public function getCalendarEvents()
    {
        return $this->calendarEvents;
    }
}
