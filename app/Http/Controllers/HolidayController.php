<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HolidayChecker;

class HolidayController extends Controller
{
    /**
     * Default page handler.
     * 
     * @return [type] [description]
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Check holiday handler.
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function check(Request $request)
    {
        $response = [
            'status' => 'FAIL',
            'message' => 'Unable to complete request.',
        ];

        try {

            if ($request->has('date')) {

                $holidayChecker = new HolidayChecker(
                    'id.indonesian%23holiday%40group.v.calendar.google.com',
                    'AIzaSyACXDmMP_wOFev-ADcnoiF044N9O0kkZ5s'
                );

                $response['message'] = $holidayChecker->checkDate(
                    $request->date
                );
                
                $response['status'] = 'OK';

            }
            else {
                $response['message'] = 'Invalid input.';
            }
            
        } catch (Exception $e) {

            $response['status'] = 'FAIL';
            $response['message'] = $e->getMessage();

        }

        return response()->json($response);

    }
}
