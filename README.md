# Simple Indonesia Holiday Checker

## About

This application will check if any given date is a Indonesia public holiday or not. 
It uses Guzzle Http library to fetch the list of holidays from Google Calendar API, map the result to array,
and finally check if any given date exists in the array.

## Technical stuffs

1. Laravel 5.5 (requires PHP >= 7.0)
2. Guzzle Http Library
3. Bootstrap 3.x & jQuery
4. Assets compiling using laravel-mix powered by webpack.

## How to run

1. Clone this repository, and change the working dir.
2. Run `composer install`
3. Sometimes you might need to change `artisan` permission to be executable `chmod +x artisan`
4. Configure `.env` file. Copy the example `cp .env.example .env`
4. Generate app key, `./artisan key:generate`
3. Run `./artisan serve` 
4. Typically it will serve at `http://127.0.0.1:8000`
